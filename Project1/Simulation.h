#pragma once

#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

using Vector3d = sf::Vector3<double>;

class Simulation
{
public:

	Simulation(const double& a, const double& b, const double& c); // Lorenz Attractor

	Simulation(const double& a, const double& b, const double& c, const double& d, const double& e, const double& f); // Aizawa Attractor

	~Simulation();

	void Render(sf::RenderWindow& l_renderWindow);

	void AddCircle();

	void SetColor(unsigned int& value);

	unsigned int GetColor();

private:

	sf::CircleShape m_circle;

	std::vector<sf::CircleShape> m_circles;

	Vector3d LorenzCircleLocation();

	Vector3d AizawaCircleLocation();

	const double dt;

	double m_a;

	double m_b;

	double m_c;

	double m_d;

	double m_e;

	double m_f;

	double m_x;

	double m_y;
	
	double m_z;

	unsigned int m_worldScale;

	unsigned int m_colorChanger;
};

