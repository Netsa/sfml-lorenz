#pragma once

#include <iostream>

#include <SFML/Graphics.hpp>

class Window
{
public:

	Window();

	Window(const std::string& l_title, const sf::Vector2u& l_windowSize);

	~Window();

	void BeginDraw();

	void Draw(sf::Drawable& l_drawable);

	void EndDraw();

	void Update();

	bool IsDone();

	sf::RenderWindow* GetRenderWindow();

public:

	void Setup(const std::string& l_title, const sf::Vector2u& l_windowSize);

	void Destroy();

	void Create();

	sf::RenderWindow m_window;

	sf::Vector2u m_windowSize;

	std::string m_windowTitle;

	bool m_isDone;

	bool m_isFullscreen;
};

