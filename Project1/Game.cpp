#include "Game.h"


Game::Game() : m_window("please kill me", sf::Vector2u(1280, 720)), m_simulation(0.95, 0.7, 0.6, 3.5, 0.25, 0.1), incrementColor(0), m_shouldDecrement(false)
{
}


Game::~Game()
{
}

void Game::HandleInput()
{
}

void Game::Update()
{
	m_window.Update();
	float resetTime = 0.05f;
	if(m_elapsed >= resetTime)
	{
		//decrement logic
		if (m_shouldDecrement)
		{
			m_simulation.SetColor(--incrementColor);
			if (m_simulation.GetColor() == 0)
				m_shouldDecrement = false;
		}
		else
		{
			m_simulation.SetColor(++incrementColor);
			if (m_simulation.GetColor() >= 255)
			{
				m_shouldDecrement = true;
			}
		}
		//////////////////
		m_simulation.AddCircle();
		m_elapsed -= resetTime;
	}

}

void Game::Render()
{
	m_window.BeginDraw();
	m_simulation.Render(*m_window.GetRenderWindow());
	m_window.EndDraw();
}

Window* Game::GetWindow()
{
	return &m_window;
}

void Game::ResetClock()
{
	m_elapsed += m_clock.restart().asSeconds();
}
