#include "Simulation.h"

Simulation::Simulation(const double& a, const double& b, const double& c)
	: m_a(a), m_b(b), m_c(c), dt(0.005), m_worldScale(8), m_x(0.01), m_y(0), m_z(0)
{
	m_circle.setFillColor(sf::Color(0));
	m_circle.setRadius(0.95f);
	m_circle.setOrigin(-640, -60);
}

Simulation::Simulation(const double& a, const double& b, const double& c, const double& d, const double& e, const double& f)
	: m_a(a), m_b(b), m_c(c), m_d(d), m_e(e), m_f(f), dt(0.01), m_worldScale(175), m_x(0.1), m_y(0), m_z(0), m_colorChanger(0)
{
	m_circle.setFillColor(sf::Color(0));
	m_circle.setRadius(0.95f);
	m_circle.setOrigin(-640, -310);
}

Simulation::~Simulation()
{
	m_circles.clear();
}

void Simulation::Render(sf::RenderWindow& l_renderWindow)
{
	for (auto& circle : m_circles)
	{
		l_renderWindow.draw(circle);
	}
}

void Simulation::AddCircle()
{
	m_circle.setPosition(AizawaCircleLocation().x, AizawaCircleLocation().y);
	m_circle.setFillColor(sf::Color(m_colorChanger, 45, 25));
	m_circles.push_back(m_circle);
	std::cout << "Per dot X:  " << AizawaCircleLocation().x << " Z: " << AizawaCircleLocation().y << AizawaCircleLocation().z << " | std::vector size: " << m_circles.size() << " Color: " << m_colorChanger << std::endl;
}

void Simulation::SetColor(unsigned int& value)
{
	m_colorChanger = value;
}

unsigned int Simulation::GetColor()
{
	return m_colorChanger;
}

Vector3d Simulation::LorenzCircleLocation()
{
	double dx = m_a * (m_y - m_x);
	double dy = m_x * (m_c - m_z) - m_y;
	double dz = m_x * m_y - m_b * m_z;

	m_x = m_x + dx * dt;
	m_y = m_y + dy * dt;
	m_z = m_z + dz * dt;

	return Vector3d(m_x * m_worldScale, m_z * m_worldScale, m_y * m_worldScale); // drawn with the Z axis as Y for better view in 2D scene
}

Vector3d Simulation::AizawaCircleLocation()
{
	double dx = (m_z - m_b) * m_x - m_d * m_y;
	double dy = m_d * m_x + (m_z - m_b) * m_y;
	double dz = m_c + m_a * m_z - (std::pow(m_z, 3) / 3) - (std::pow(m_x, 2) + std::pow(m_y, 2)) * (1 + m_e * m_z) + m_f * m_z * std::pow(m_x, 3);

	m_x = m_x + dx * dt;
	m_y = m_y + dy * dt;
	m_z = m_z + dz * dt;

	return Vector3d(m_x * m_worldScale, m_z * m_worldScale, m_y * m_worldScale); // y and z swapped for better view
}
