#pragma once

#include <SFML/Graphics.hpp>

#include "Window.h"
#include "Simulation.h"

class Game
{
public:

	Game();

	~Game();

public:

	void HandleInput();

	void Update();

	void Render();

	Window* GetWindow();

	void ResetClock();

private:

	Window m_window;

	sf::CircleShape m_circle;

	Simulation m_simulation;

	sf::Clock m_clock;

	float m_elapsed;

	unsigned int incrementColor;
	
	bool m_shouldDecrement;

};

